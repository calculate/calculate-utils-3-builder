# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from ..datavars import BuilderError
from calculate.lib.utils.git import GitError
from calculate.install.distr import DistributiveError

_ = lambda x: x
setLocalTranslate('cl_builder3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClBuilderPrepareAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (DistributiveError, FilesError,
                    TemplatesError,
                    BuilderError, GitError)

    successMessage = __("Build prepared successfully in \"{cl_builder_path}\"")
    failedMessage = __("Preparing the build is failed")
    interruptMessage = __("Preparing manually interrupted")

    # список задач для действия
    tasks = [
        # форматирование раздела при необходимости
        {'name': 'format',
         'message': __("Formatting the partitions"),
         'method': 'Install.format(cl_builder_target)',
         'condition': lambda Get: Get('cl_builder_target').needFormat
         },
        # распаковка дистрибутива
        {'name': 'unpack',
         'message': __("Unpacking the system image to the target"),
         'method': 'Install.unpack(cl_builder_source,cl_builder_target,'
                   'os_builder_linux_files)',
         },
        # подмонтировать необходимые данные
        {'name': 'mount',
         'message': __("Mounting resources"),
         'method': 'Builder.mount_target(cl_builder_target)',
         },
        {'name': 'apply_template',
         'message': __("Configuring build"),
         # наложить шаблоны в развернутый дистрибутив
         'method': 'Builder.applyTemplates(cl_builder_target,False,False,None)',
         },
        # save
        {'name': 'save',
         'method': 'Builder.save_build(cl_builder_build,'
                   'cl_builder_linux_datavars)',
         },
        # закрепить подключенные данные
        {'name': 'detach',
         'method': 'Builder.detach_target(cl_builder_target)',
         },
        {'name': 'set_caps',
         'method': 'Builder.restore_file_capabilities()',
         },
        {'name': 'close_build',
         'warning': _("Umount distributives"),
         'method': 'Builder.close_build(cl_builder_build)',
         'depend': Tasks.failed(),
         'condition': lambda Get: Get('cl_builder_build')
         }]
