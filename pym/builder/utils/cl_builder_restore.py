# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from calculate.lib.utils.mount import isMount
from ..datavars import BuilderError
from calculate.lib.utils.git import GitError
from calculate.install.distr import DistributiveError

_ = lambda x: x
setLocalTranslate('cl_builder3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClBuilderRestoreAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (DistributiveError, FilesError,
                    TemplatesError,
                    BuilderError, GitError)

    successMessage = __("Build was restored successfully")
    failedMessage = None
    interruptMessage = __("Restoring manually interrupted")

    # список задач для действия
    tasks = [
        {'name': 'check_build_run',
         'method': 'Builder.check_build_run()'},
        {'name': 'check_chroot_run',
         'method': 'Builder.check_chroot_run()'},
        {'name': 'restore_build',
         'message': __("Restoring {cl_builder_id} build"),
         'method': 'Builder.restore_build(cl_builder_build)',
         'condition': lambda Get: not isMount(Get('cl_builder_path'))
         },
        # подмонтировать необходимые данные
        {'name': 'mount',
         'message': __("Mounting build resources"),
         'method': 'Builder.mount_target(cl_builder_target)',
         },
        # save
        {'name': 'save',
         'message': __("Save build information"),
         'method': 'Builder.save_build(cl_builder_build,'
                   'cl_builder_linux_datavars)',
         },
        # закрепить подключенные данные
        {'name': 'detach',
         'method': 'Builder.detach_target(cl_builder_target)',
         },
        {'name': 'failed',
         'error': __("Restoring the build is failed"),
         'depend': (Tasks.failed() & Tasks.hasnot("interrupt") &
                    Tasks.success_all("check_build_run"))
         },
    ]
