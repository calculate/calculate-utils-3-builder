# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from calculate.lib.variables.system import RootType
from calculate.update.emerge_parser import EmergeError
from calculate.update.update import UpdateError
from calculate.lib.utils.git import GitError
from calculate.install.distr import DistributiveError
from ..datavars import BuilderError
from ..variables.action import Actions 

_ = lambda x: x
setLocalTranslate('cl_builder3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClBuilderMenuAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (DistributiveError, FilesError, UpdateError,
                    TemplatesError, BuilderError, GitError, EmergeError)

    successMessage = __("Boot menu updated successfully")
    failedMessage = __("Failed to update boot menu")
    interruptMessage = __("Boot menu updating manually interrupted")

    # список задач для действия
    tasks = [
        {'name': 'flash',
         'condition': lambda Get: Get('cl_builder_livemenu_type') == 'flash'
         },
        {'name': 'grub',
         'condition': lambda Get: Get('cl_builder_livemenu_type') != 'flash'
         },
        {'name': 'grub:grub_image_menu',
         'method': 'Builder.set_builder_action("%s")' % Actions.ImageMenu,
         },
        {'name': 'grub:mount_data_rw',
         'method': 'Builder.remount_rw(cl_isoscan_base_path)',
         'condition': lambda Get: (
             Get('os_root_type_ext') in RootType.IsoScanGrub)
         },
        {'name': 'grub:update_grub_menu',
         'message': __("Creating the Live HDD menu"),
         'method': 'Builder.create_iso_grub_cfg(cl_builder_livemenu_path)',
         'condition': lambda Get: (
             Get('builder.cl_builder_livemenu_path') != "")
         },
        {'name': 'grub:clear_grub_menu',
         'message': __("Clearing the ISO images menu"),
         'method': 'Builder.clear_iso_grub_cfg(cl_livemenu_path)',
         'condition': lambda Get: (
             Get('os_root_type_ext') in RootType.HDDs and
             Get('builder.cl_builder_livemenu_path') == "")
         },
        {'name': 'grub:setup_grub',
         'message': __("Configuration grub for {cl_builder_livemenu_path}"),
         'method': 'Builder.setup_package("sys-boot/grub")',
         'condition': lambda Get: (Get('os_root_type_ext') in RootType.HDDs and
                                   Get('builder.cl_builder_livemenu_path') !=
                                   Get('builder.cl_livemenu_path') and
                                   Get('builder.cl_builder_livemenu_path'))
         },
        {'name': 'grub:setup_grub_empty',
         'message': __("Disabling the Live HDD menu from grub"),
         'method': 'Builder.setup_package("sys-boot/grub")',
         'condition': lambda Get: (Get('os_root_type_ext') in RootType.HDDs and
                                   Get('builder.cl_builder_livemenu_path') !=
                                   Get('builder.cl_livemenu_path') and
                                   not Get('builder.cl_builder_livemenu_path'))
         },
        {'name': 'flash:mount_flash',
         'method': 'Builder.remount_rw(cl_builder_flash_path)',
         },
        {'name': 'flash:iso_linux_migrate',
         'method': 'Builder.iso_migrate(cl_builder_flash_path)'
         },
        {'name': 'flash:protect_off',
         'method': 'Builder.setVariable("cl_protect_use_set","off",True)'
         },
        {'name': 'flash:image_menu',
         'method': 'Builder.set_builder_action("%s")' % Actions.ImageMenu,
         },
        {'name': 'image_menu:sync_vmlinuz',
         'message': __("Extracting kernels from ISO images"),
         'method': 'Builder.sync_vmlinuz(cl_builder_flash_path)'
         },
        {'name': 'image_menu:update_menu',
         'message': __("Recreating the Live USB menu"),
         'method': 'Builder.update_menu(cl_builder_flash_path)',
         },
    ]
