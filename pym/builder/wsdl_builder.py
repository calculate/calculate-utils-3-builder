# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys

from calculate.lib.datavars import VariableError, DataVarsError

from calculate.core.server.func import WsdlBase
from calculate.install.install import InstallError, Install
from calculate.update.emerge_parser import EmergeError
from .builder import Builder
from calculate.update.update import UpdateError
from .datavars import BuilderError
from .variables.action import Actions as BuilderActions
from calculate.lib.utils.git import GitError
from .utils.cl_builder_prepare import ClBuilderPrepareAction
from .utils.cl_builder_profile import ClBuilderProfileAction
from .utils.cl_builder_break import ClBuilderBreakAction
from .utils.cl_builder_update import ClBuilderUpdateAction
from .utils.cl_builder_restore import ClBuilderRestoreAction
from .utils.cl_builder_image import ClBuilderImageAction
from .utils.cl_builder_menu import ClBuilderMenuAction

_ = lambda x: x
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

setLocalTranslate('cl_builder3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        #
        # Подготовить систему для сборки
        #
        {
            # идентификатор метода
            'method_name': Builder.Method.Prepare,
            # категория метода
            'category': __('Builder'),
            # заголовок метода  Подготовить новую сборку
            'title': __("Prepare the New Build"),
            # иконка для графической консоли
            'image': 'calculate-builder-prepare,starred,rating,gtk-about',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-prepare',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderPrepareAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Prepare,
                        'cl_dispatch_conf_default': 'usenew'},
            # описание груп (список лямбда функций)
            'groups': [
                # Подготовить новую сборку
                lambda group: group(
                    _("Prepare the New Build"),
                    # Исходный образ (Source image)
                    normal=('cl_builder_source_filename',),
                    # Идентификатор сборки (Build ID)
                    expert=(  # Место сборки
                              # (Build location)
                              'cl_builder_disk_dev',
                              # Использовать слои для сборки
                              # Use layers for build
                              'cl_builder_layered_set',
                              'cl_builder_new_id',
                              'cl_templates_locate',
                              'cl_verbose_set',
                              'cl_dispatch_conf'),
                    hide=('cl_templates_locate',
                          'cl_verbose_set',
                          'cl_builder_disk_dev',
                          'cl_builder_layered_set',
                          'cl_builder_new_id',
                          'cl_dispatch_conf'),
                    brief=('cl_builder_source_filename',
                           'cl_builder_profile_name',
                           'cl_builder_disk_dev',
                           'cl_builder_new_id',
                           'cl_builder_layered_set',
                           'cl_builder_disk_size',
                           ),
                    next_label=_("Next"))],
            'brief': {'next': __("Run"),
                      'name': __("Prepare the New Build")},
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.Break,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Break the Build"),
            # иконка для графической консоли
            'image': 'calculate-builder-break,edit-clear',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-break',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderBreakAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Break},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Break the Build"),
                                    normal=('cl_builder_prepared_id',
                                            'cl_builder_clear_set',
                                            'cl_builder_clear_pkg_set'),
                                    brief=('os_builder_linux_fullname',),
                                    next_label=_("Run"))],
            'brief': {'next': __("Run"),
                      'name': __("Break the Build")},
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.Update,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Update the Build"),
            # иконка для графической консоли
            'image': 'calculate-builder-update,software-update-available,system-run',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-update',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderUpdateAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Update,
                        'cl_dispatch_conf_default': 'usenew'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Update the Build"),
                    normal=('cl_builder_prepared_id',
                            'cl_builder_binhost_stable_opt_set',
                            'update.cl_update_binhost_recheck_set',
                            'cl_builder_with_bdeps_opt_set',),
                    expert=(
                        'update.cl_update_binhost_choice',
                        'update.cl_update_rep_hosting_choice',
                        'update.cl_update_sync_only_set',
                        'update.cl_update_other_set',
                        'update.cl_update_pretend_set',
                        'cl_builder_sync_rep',
                        'update.cl_update_emergelist_set',
                        'update.cl_update_check_rep_set',
                        'update.cl_update_world',
                        'builder.cl_builder_gpg_force',
                        'update.cl_update_egencache_force',
                        'update.cl_update_eixupdate_force',
                        'cl_builder_rebuild_changed_set',
                        'cl_builder_rebuilt_binaries_set',
                        'update.cl_update_revdep_rebuild_set',
                        'update.cl_update_onedepth_set',
                        'update.cl_update_cleanpkg_set',
                        'builder.cl_builder_check_automagic_set',
                        'cl_builder_branch_data',
                        'cl_templates_locate',
                        'cl_verbose_set',
                        'cl_dispatch_conf'),
                    hide=(
                        'cl_builder_prepared_id',
                        'cl_builder_binhost_stable_opt_set',
                        'update.cl_update_binhost_recheck_set',
                        'update.cl_update_sync_only_set',
                        'update.cl_update_other_set',
                        'update.cl_update_pretend_set',
                        'cl_builder_sync_rep',
                        'builder.cl_builder_gpg_force',
                        'update.cl_update_emergelist_set',
                        'update.cl_update_check_rep_set',
                        'update.cl_update_world',
                        'update.cl_update_egencache_force',
                        'update.cl_update_eixupdate_force',
                        'cl_builder_rebuild_changed_set',
                        'cl_builder_rebuilt_binaries_set',
                        'update.cl_update_revdep_rebuild_set',
                        'update.cl_update_onedepth_set',
                        'update.cl_update_cleanpkg_set',
                        'builder.cl_builder_check_automagic_set',
                        'cl_builder_branch_data',
                        'cl_templates_locate',
                        'cl_verbose_set',
                        'cl_builder_branch_data',
                        'cl_dispatch_conf'),
                    brief=(
                        'cl_builder_prepared_id',
                        'cl_builder_binhost_stable_opt_set',
                        'update.cl_update_binhost_recheck_set',
                        'cl_builder_with_bdeps_opt_set',
                        'update.cl_update_sync_only_set',
                        'update.cl_update_other_set',
                        'update.cl_update_pretend_set',
                        'cl_builder_sync_rep',
                        'update.cl_update_emergelist_set',
                        'update.cl_update_check_rep_set',
                        'update.cl_update_world',
                        'builder.cl_builder_gpg_force',
                        'update.cl_update_egencache_force',
                        'update.cl_update_eixupdate_force',
                        'cl_builder_rebuild_changed_set',
                        'cl_builder_rebuilt_binaries_set',
                        'update.cl_update_revdep_rebuild_set',
                        'update.cl_update_onedepth_set',
                        'update.cl_update_cleanpkg_set',
                        'builder.cl_builder_check_automagic_set',
                        'cl_builder_brief_branch_data',
                    ),
                    next_label=_("Next"))],
            'brief': {'next': __("Run"),
                      'name': __("Update the Build")}
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.UpdateMenu,
            # категория метода
            'category': __("Configuration"),
            # заголовок метода
            'title': __("Configure a Live-Menu"),
            # иконка для графической консоли
            'image': 'calculate-update-livemenu,format-indent-less-rtl,'
                     'format-indent-more',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-setup-boot-live',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderMenuAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Image},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Update Boot Menu"),
                                    normal=(
                                        'cl_builder_livemenu_type',
                                        'cl_builder_livemenu_path',),
                                    expert=(
                                        'cl_builder_locale_lang',
                                        'cl_builder_timezone',
                                        'cl_builder_x11_video_drv',
                                        'cl_builder_audio',
                                        'cl_builder_x11_resolution',
                                        'cl_builder_x11_composite',
                                        'cl_builder_docache_set'
                                    ),
                                    next_label=_("Run"))],
            'brief': {'next': __("Run"),
                      'name': __("Update Live-Menu")},
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.Restore,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Restore the Build"),
            # иконка для графической консоли
            'image': 'calculate-builder-restore,kt-restore-defaults,'
                     'document-revert-rtl,'
                     'bookmark-new-list,document-revert',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-restore',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderRestoreAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Restore},
            # описание груп (список лямбда функций)
            'groups': [
                # there used to be a typo: Break the Build instead of Restore the Build
                lambda group: group(_("Restore the Build"),
                                    normal=('cl_builder_broken_id',),
                                    expert=('cl_templates_locate',
                                            'cl_verbose_set',
                                            'cl_dispatch_conf'),
                                    next_label=_("Run"))],
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.Container,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Create the Container"),
            # иконка для графической консоли
            'image': 'calculate-builder-image,media-optical,media-optical-data',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-container',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderImageAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError, EmergeError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Image,
                        'cl_builder_image_type': 'container',
                        'cl_dispatch_conf_default': 'usenew'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Create the Container"),
                                    normal=('cl_builder_prepared_id',
                                            'cl_builder_image_filename',
                                            ),
                                    brief=('cl_builder_prepared_id',
                                           'os_builder_linux_fullname',
                                           'os_builder_linux_shortname',
                                           'os_builder_linux_subname',
                                           'os_builder_linux_system',
                                           'os_builder_linux_ver',
                                           'os_builder_linux_build',
                                           'cl_builder_image_filename',
                                           'cl_builder_keep_tree_set',
                                           'cl_builder_prelink_set',
                                           'cl_builder_container_free_size'
                                           ),
                                    hide=('cl_templates_locate',
                                          'cl_verbose_set',
                                          'cl_dispatch_conf'
                                          'cl_builder_prepared_id',
                                          'cl_builder_image_filename',
                                          'cl_builder_keep_tree_set',
                                          'cl_builder_prelink_set',
                                          ),
                                    expert=(
                                        'cl_builder_keep_tree_set',
                                        'cl_builder_prelink_set',
                                        'cl_builder_binhost_stable_set',
                                        'cl_templates_locate',
                                        'cl_verbose_set',
                                        'cl_dispatch_conf'),
                                    next_label=_("Next"))],
            'brief': {'next': __("Run"),
                      'name': __("Create the Container")}
        },
        {
            # идентификатор метода
            'method_name': Builder.Method.Image,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Create the Image"),
            # иконка для графической консоли
            'image': 'calculate-builder-image,media-optical,media-optical-data',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-image',
            # права для запуска метода
            'rights': ['build'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder,
                      'Install': Install},
            # описание действия
            'action': ClBuilderImageAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError, EmergeError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.Image,
                        'cl_builder_image_type': 'iso',
                        'cl_dispatch_conf_default': 'usenew'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Create the Image"),
                                    normal=('cl_builder_prepared_id',
                                            'cl_builder_image_filename',
                                            ),
                                    brief=('cl_builder_prepared_id',
                                           'os_builder_linux_fullname',
                                           'os_builder_linux_shortname',
                                           'os_builder_linux_subname',
                                           'os_builder_linux_system',
                                           'os_builder_linux_build',
                                           'cl_builder_image_filename',
                                           'cl_builder_videodrv_set',
                                           'cl_builder_compress',
                                           'cl_builder_isohybrid_set',
                                           'cl_builder_keep_tree_set',
                                           'cl_builder_prelink_set',
                                           'cl_builder_update_themes_set',
                                           'cl_builder_prepare_free_size',
                                           'cl_builder_image_free_size'
                                           ),
                                    hide=('cl_templates_locate',
                                          'cl_verbose_set',
                                          'cl_dispatch_conf'
                                          'cl_builder_prepared_id',
                                          'cl_builder_image_filename',
                                          'cl_builder_videodrv_set',
                                          'cl_builder_compress',
                                          'cl_builder_update_themes_set',
                                          'cl_builder_keep_tree_set',
                                          'cl_builder_isohybrid_set',
                                          'cl_builder_prelink_set',
                                          ),
                                    expert=(
                                        # Включить проприетарные видео драйвера
                                        # Include proprietary video drivers
                                        'cl_builder_videodrv_set',
                                        'cl_builder_keep_tree_set',
                                        'cl_builder_isohybrid_set',
                                        'cl_builder_prelink_set',
                                        'cl_builder_compress',
                                        'cl_builder_binhost_stable_set',
                                        'cl_builder_update_themes_set',
                                        'cl_templates_locate',
                                        'cl_verbose_set',
                                        'cl_dispatch_conf'),
                                    next_label=_("Next"))],
            'brief': {'next': __("Run"),
                      'name': __("Create the Image")}
        },
        #
        # Сменить профиль
        #
        {
            # идентификатор метода
            'method_name': Builder.Method.Profile,
            # категория метода
            'category': __('Builder'),
            # заголовок метода
            'title': __("Change the Build Profile"),
            # иконка для графической консоли
            'image': 'calculate-builder-profile,'
                     'notification-display-brightness-full,gtk-dialog-info,'
                     'help-hint',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-builder-profile',
            # права для запуска метода
            'rights': ['builder'],
            # объект содержащий модули для действия
            'logic': {'Builder': Builder},
            # описание действия
            'action': ClBuilderProfileAction,
            # объект переменных
            'datavars': "builder",
            'native_error': (VariableError, DataVarsError, UpdateError,
                             InstallError, BuilderError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': BuilderActions.ChangeProfile,
                        'update.cl_update_world_default': 'rebuild'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Repository"),
                    brief=('cl_builder_profile_repo_name',),
                    hide=('cl_builder_profile_url',
                          'update.cl_update_profile_sync_set'),
                    normal=('cl_builder_prepared_id',
                            'cl_builder_profile_url',),
                    expert=('update.cl_update_profile_sync_set',)),
                lambda group: group(
                    _("Profile"),
                    normal=('cl_builder_profile_system_shortname',
                            'update.cl_update_world'),
                    expert=('update.cl_update_skip_setup_set',
                            'cl_builder_templates_locate',
                            'cl_verbose_set',
                            'cl_dispatch_conf'),
                    hide=('cl_builder_templates_locate',
                          'cl_verbose_set',
                          'cl_dispatch_conf'),
                    brief=('cl_builder_profile_linux_fullname',
                           'cl_builder_profile_depend_data')
                )],
            'brief': {'next': __("Run"),
                      'name': __("Set the Profile")}},
    ]
