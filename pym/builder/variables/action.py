# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import ActionVariable

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_builder3', sys.modules[__name__])


class Actions():
    Prepare = "prepare"
    Image = "image"
    Break = "break"
    Restore = "restore"
    Update = "assemble_update"
    ChangeProfile = "builder_profile"
    All = (Prepare, Image, Break, Restore, Update, ChangeProfile)

    NewAssemble = (Prepare,)
    WorkAssemble = (Image, Break, ChangeProfile, Update)
    BrokenAssemble = (Restore,)

    ImageSquash = "squash"
    ImageIso = "iso"
    ImageContainer = "container"
    ImageMenu = "menu"
    PrepareAssemble = "prepare"


class VariableAcBuilderSquash(ActionVariable):
    """
    Action variable which has value "on" for prepare squash
    """

    def action(self, cl_action):
        image_action = self.Get('cl_builder_action')
        if cl_action == Actions.Image and image_action == Actions.ImageSquash:
            return "on"
        return "off"


class VariableAcBuilderIso(ActionVariable):
    """
    Action variable which has value "on" for prepare iso
    """

    def action(self, cl_action):
        image_action = self.Get('cl_builder_action')
        if cl_action == Actions.Image and image_action in (Actions.ImageIso,
                                                           Actions.ImageMenu):
            return "on"
        return "off"


class VariableAcBuilderContainer(ActionVariable):
    """
    Действие выполняемое при подготовке метаданных контейнера
    """

    def action(self, cl_action):
        image_action = self.Get('cl_builder_action')
        if (cl_action == Actions.Image and
                image_action in (Actions.ImageContainer,)):
            return "on"
        return "off"


class VariableAcBuilderSetup(ActionVariable):
    """
    Action variable which has value "on" for setup build directory
    """

    def action(self, cl_action):
        if (cl_action in (Actions.Prepare, Actions.Update) and
                not self.GetBool('cl_builder_stage_set')):
            return "on"
        image_action = self.Get('cl_builder_action')
        if (cl_action in (Actions.Image,) and
                image_action == Actions.PrepareAssemble):
            return "on"
        return "off"


class VariableAcBuilderPrepare(ActionVariable):
    """
    Action variable which has value "on" for setup build directory
    """

    def action(self, cl_action):
        if cl_action in (Actions.Prepare, Actions.Update):
            return "on"
        image_action = self.Get('cl_builder_action')
        if (cl_action in (Actions.Image,) and
                image_action == Actions.PrepareAssemble):
            return "on"
        return "off"
